﻿using AutoMapper;
using LogiData.Web.Models;
using MyLogiscool.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace LogiData.Web.Controllers
{
    public class OwnersApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        ILogic logic;
        IMapper mapper;
        public OwnersApiController()
        {
            logic = new MainLogic();
            mapper = MapperFactory.CreateMapper();
        }

        // XML...
        [ActionName("all")]
        [HttpGet]
        // GET api/OwnersApi/all
        public IEnumerable<Models.Owner> GetAll()
        {
            var owners = logic.ReadAllOwners();
            return mapper.Map<IEnumerable<MyLogiscool.Data.Owners>, List<Models.Owner>>(owners);
        }

        // GET api/OwnersApi/del/42
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneOwner(int id)
        {
            logic.DeleteOwners(id);
            return new ApiResult() { OperationResult = true };
        }

        // POST api/OwnersApi/add + owner
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneOwner(Owner owner)
        {
            logic.CreateOwners(mapper.Map<Models.Owner,MyLogiscool.Data.Owners>(owner));
            return new ApiResult() { OperationResult = true };
        }
        // POST api/OwnersApi/mod + owner
        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneOwner(Owner owner)
        {
            logic.UpdateOwners(mapper.Map<Models.Owner, MyLogiscool.Data.Owners>(owner));
            return new ApiResult() { OperationResult = true };
        }
    }
}
