﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace LogiData.ConsoleClient
{
    // TODO: NSwag / SwashBuckle
    // Api Testing: Selenium, Postman
    // NuGet: Newtonsoft.JSON
    // TODO: multiple startup projects
    public class Owner
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string City { get; set; }
        public int StartYear { get; set; }
        public bool HasPaidThisYear { get; set; }
        public bool IsReplaceable { get; set; }
        public override string ToString()
        {
            return $"ID={ID}\tName={Name}\tCity={City}\tStartYear={StartYear}\tHasPaidThisYear={HasPaidThisYear}\tIsReplaceable={IsReplaceable}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Várakozok......");
            Console.ReadLine();

            string url = "http://localhost:43624/api/OwnersApi/";

            // WebClient => HttpClient
            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Owner>>(json);
                foreach (var item in list) Console.WriteLine(item);
                Console.ReadLine();

                // WebClient => NameValueCollection postData + byte[] responseBytes
                Dictionary<string, string> postData;
                string response;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Owner.Name), "Keszeg Ferenc");
                postData.Add(nameof(Owner.City), "Kolozsvár");
                postData.Add(nameof(Owner.StartYear), "1770");
                postData.Add(nameof(Owner.IsReplaceable), "true");
                postData.Add(nameof(Owner.HasPaidThisYear), "true");

                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int ownerId = JsonConvert.DeserializeObject<List<Owner>>(json).First(t => t.Name == "Keszeg Ferenc").ID;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Owner.ID), ownerId.ToString());
                postData.Add(nameof(Owner.Name), "Keszeg Ferenc");
                postData.Add(nameof(Owner.City), "Győr");
                postData.Add(nameof(Owner.HasPaidThisYear), "true");
                postData.Add(nameof(Owner.IsReplaceable), "false");

                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                response = client.GetStringAsync(url + "del/" + ownerId).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
            }

        }
    }
}

