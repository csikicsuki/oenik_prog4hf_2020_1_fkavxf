﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogiData.Wpf
{
    class OwnerVM : ObservableObject
    {
		private int id;
		private string name;
		private string city;
		private int startYear;
		private	bool hasPaidThisYear;
		private bool isReplaceable;

		public bool IsReplaceable
		{
			get { return isReplaceable; }
			set { Set(ref isReplaceable, value); }
		}


		public bool HasPaidThisYear
		{
			get { return hasPaidThisYear; }
			set { Set(ref hasPaidThisYear, value); }
		}


		public int StartYear
		{
			get { return startYear; }
			set { Set(ref startYear, value); }
		}


		public string City
		{
			get { return city; }
			set { Set(ref city, value); }
		}

		public string Name
		{
			get { return name; }
			set { Set( ref name, value); }
		}
		public int Id
		{
			get { return id; }
			set { Set(ref id, value); }
		}

		public void CopyFrom(OwnerVM other)
		{
			if (other == null)
			{
				return;
			}
			this.Id = other.Id;
			this.Name = other.Name;
			this.City = other.City;
			this.StartYear = other.StartYear;
			this.HasPaidThisYear = other.HasPaidThisYear;
			this.IsReplaceable = other.IsReplaceable;
		}

	}
}
