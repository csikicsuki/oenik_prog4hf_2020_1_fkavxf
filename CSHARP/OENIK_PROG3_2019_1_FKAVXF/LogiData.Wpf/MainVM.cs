﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace LogiData.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
		private OwnerVM selectedOwner;
		private ObservableCollection<OwnerVM> allOwners;

		public ObservableCollection<OwnerVM> AllOwners
		{
			get { return allOwners; }
			set { Set(ref allOwners, value); }
		}


		public OwnerVM SelectedOwner
		{
			get { return selectedOwner; }
			set { Set(ref selectedOwner,value); }
		}

		public ICommand AddCmd { get; private set; }
		public ICommand DelCmd { get; private set; }
		public ICommand ModCmd { get; private set; }
		public ICommand LoadCmd { get; private set; }

		public Func<OwnerVM,bool> EditorFunc { get; set; } 

		public MainVM()
		{
			logic = new MainLogic();

			DelCmd = new RelayCommand(() => logic.ApiDelOwner(selectedOwner));
			AddCmd = new RelayCommand(() => logic.EditOwner(null,EditorFunc));
			ModCmd = new RelayCommand(() => logic.EditOwner(selectedOwner,EditorFunc));
			LoadCmd = new RelayCommand(() => AllOwners = new ObservableCollection<OwnerVM>(logic.ApiGetOwners()));
		}

	}
}
