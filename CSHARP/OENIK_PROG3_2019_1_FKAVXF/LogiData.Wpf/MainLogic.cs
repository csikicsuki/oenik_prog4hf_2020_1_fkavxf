﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LogiData.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:43624/api/OwnersApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed succesfully" : "Operation failed";
            Messenger.Default.Send(msg, "OwnerResult");
        }

        public List<OwnerVM> ApiGetOwners ()
        {
            
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<OwnerVM>>(json);
            //SendMessage(true);
            return list;
        }

        public void ApiDelOwner(OwnerVM owner)
        {
            bool success = false;
            if (owner != null)
            {
                string json = client.GetStringAsync(url + "del/" + owner.Id).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditOwner(OwnerVM owner,bool isEditing)
        {
            if (owner == null) return false;
            string myUrl = isEditing ? url + "mod" : url + "add";
            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add(nameof(OwnerVM.Id), owner.Id.ToString());
            }
            postData.Add(nameof(OwnerVM.Name), owner.Name);
            postData.Add(nameof(OwnerVM.City), owner.City);
            postData.Add(nameof(OwnerVM.StartYear), owner.StartYear.ToString());
            postData.Add(nameof(OwnerVM.HasPaidThisYear), owner.HasPaidThisYear.ToString());
            postData.Add(nameof(OwnerVM.IsReplaceable), owner.IsReplaceable.ToString());

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditOwner(OwnerVM owner, Func<OwnerVM,bool> editor)
        {
            OwnerVM temp = new OwnerVM();
            if (owner != null)
            {
                temp.CopyFrom(owner);
            }
            bool? success = editor?.Invoke(temp);
            if (success == true)
            {
                if (owner != null)
                {
                    success = ApiEditOwner(temp, true);
                }
                else
                {
                    success = ApiEditOwner(temp, false);
                }
            }
            SendMessage(success == true);
        }
    }
}
